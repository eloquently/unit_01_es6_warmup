Install all dependencies:

```
npm install
```

Run webpack:

```
npm run webpack
```

Automatically run webpack when files change:

```
npm run webpack:watch
```

Run tests:

```
npm run test
```

Run tests automatically when files change:

```
npm run test:watch
```